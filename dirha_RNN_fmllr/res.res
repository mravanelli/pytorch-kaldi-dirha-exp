ep=000 tr=['tr05_cont'] loss=4.020 err=0.715 valid=dirha_sim loss=2.792 err=0.602 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=692
ep=001 tr=['tr05_cont'] loss=2.422 err=0.586 valid=dirha_sim loss=2.505 err=0.557 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=722
ep=002 tr=['tr05_cont'] loss=2.160 err=0.540 valid=dirha_sim loss=2.335 err=0.525 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=845
ep=003 tr=['tr05_cont'] loss=2.029 err=0.516 valid=dirha_sim loss=2.295 err=0.518 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=902
ep=004 tr=['tr05_cont'] loss=1.945 err=0.500 valid=dirha_sim loss=2.254 err=0.507 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=901
ep=005 tr=['tr05_cont'] loss=1.875 err=0.486 valid=dirha_sim loss=2.209 err=0.498 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=909
ep=006 tr=['tr05_cont'] loss=1.822 err=0.476 valid=dirha_sim loss=2.201 err=0.498 lr_architecture1=0.000320 lr_architecture2=0.000400 time(s)=910
ep=007 tr=['tr05_cont'] loss=1.731 err=0.457 valid=dirha_sim loss=2.154 err=0.488 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=898
ep=008 tr=['tr05_cont'] loss=1.697 err=0.449 valid=dirha_sim loss=2.137 err=0.485 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=945
ep=009 tr=['tr05_cont'] loss=1.674 err=0.444 valid=dirha_sim loss=2.141 err=0.483 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=924
ep=010 tr=['tr05_cont'] loss=1.652 err=0.440 valid=dirha_sim loss=2.130 err=0.481 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=908
ep=011 tr=['tr05_cont'] loss=1.630 err=0.435 valid=dirha_sim loss=2.128 err=0.482 lr_architecture1=0.000160 lr_architecture2=0.000200 time(s)=917
ep=012 tr=['tr05_cont'] loss=1.590 err=0.427 valid=dirha_sim loss=2.105 err=0.477 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=914
ep=013 tr=['tr05_cont'] loss=1.576 err=0.424 valid=dirha_sim loss=2.104 err=0.476 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=920
ep=014 tr=['tr05_cont'] loss=1.566 err=0.422 valid=dirha_sim loss=2.106 err=0.476 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=924
ep=015 tr=['tr05_cont'] loss=1.553 err=0.419 valid=dirha_sim loss=2.095 err=0.474 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=920
ep=016 tr=['tr05_cont'] loss=1.543 err=0.417 valid=dirha_sim loss=2.097 err=0.473 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=895
ep=017 tr=['tr05_cont'] loss=1.534 err=0.415 valid=dirha_sim loss=2.083 err=0.471 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=916
ep=018 tr=['tr05_cont'] loss=1.524 err=0.413 valid=dirha_sim loss=2.086 err=0.471 lr_architecture1=0.000080 lr_architecture2=0.000100 time(s)=938
ep=019 tr=['tr05_cont'] loss=1.507 err=0.409 valid=dirha_sim loss=2.090 err=0.472 lr_architecture1=0.000040 lr_architecture2=0.000050 time(s)=918
ep=020 tr=['tr05_cont'] loss=1.496 err=0.407 valid=dirha_sim loss=2.081 err=0.470 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=942
ep=021 tr=['tr05_cont'] loss=1.492 err=0.406 valid=dirha_sim loss=2.077 err=0.469 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=926
ep=022 tr=['tr05_cont'] loss=1.489 err=0.405 valid=dirha_sim loss=2.073 err=0.468 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=936
ep=023 tr=['tr05_cont'] loss=1.486 err=0.404 valid=dirha_sim loss=2.078 err=0.469 lr_architecture1=0.000020 lr_architecture2=0.000025 time(s)=916
%WER 25.0 | 409 6762 | 77.2 16.1 6.6 2.2 25.0 78.7 | /scratch/ravanelm/pytorch-kaldi-new/exp/dirha_RNN_fmllr_3lay/decode_dirha_real_out_dnn2/score_16/ctm.filt.sys

