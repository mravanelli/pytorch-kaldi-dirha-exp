ep=000 tr=['tr05_cont'] loss=3.592 err=0.698 valid=dirha_sim loss=2.817 err=0.584 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1799
ep=001 tr=['tr05_cont'] loss=2.161 err=0.513 valid=dirha_sim loss=2.235 err=0.502 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1998
ep=002 tr=['tr05_cont'] loss=1.714 err=0.438 valid=dirha_sim loss=2.015 err=0.466 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2499
ep=003 tr=['tr05_cont'] loss=1.529 err=0.405 valid=dirha_sim loss=1.966 err=0.457 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2672
ep=004 tr=['tr05_cont'] loss=1.424 err=0.383 valid=dirha_sim loss=1.915 err=0.450 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2714
ep=005 tr=['tr05_cont'] loss=1.341 err=0.366 valid=dirha_sim loss=1.879 err=0.437 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2712
ep=006 tr=['tr05_cont'] loss=1.278 err=0.352 valid=dirha_sim loss=1.855 err=0.433 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2736
ep=007 tr=['tr05_cont'] loss=1.217 err=0.339 valid=dirha_sim loss=1.838 err=0.430 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2729
ep=008 tr=['tr05_cont'] loss=1.169 err=0.329 valid=dirha_sim loss=1.836 err=0.427 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2738
ep=009 tr=['tr05_cont'] loss=1.125 err=0.319 valid=dirha_sim loss=1.812 err=0.421 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2727
ep=010 tr=['tr05_cont'] loss=1.089 err=0.311 valid=dirha_sim loss=1.830 err=0.425 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=2735
ep=011 tr=['tr05_cont'] loss=0.996 err=0.287 valid=dirha_sim loss=1.774 err=0.412 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=2721
ep=012 tr=['tr05_cont'] loss=0.969 err=0.281 valid=dirha_sim loss=1.778 err=0.413 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=2733
ep=013 tr=['tr05_cont'] loss=0.923 err=0.268 valid=dirha_sim loss=1.755 err=0.407 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=2699
ep=014 tr=['tr05_cont'] loss=0.909 err=0.265 valid=dirha_sim loss=1.752 err=0.408 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=2746
ep=015 tr=['tr05_cont'] loss=0.886 err=0.258 valid=dirha_sim loss=1.745 err=0.406 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=2749
ep=016 tr=['tr05_cont'] loss=0.879 err=0.257 valid=dirha_sim loss=1.744 err=0.405 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=2786
ep=017 tr=['tr05_cont'] loss=0.873 err=0.255 valid=dirha_sim loss=1.741 err=0.404 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=2803
ep=018 tr=['tr05_cont'] loss=0.870 err=0.254 valid=dirha_sim loss=1.746 err=0.405 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=2835
ep=019 tr=['tr05_cont'] loss=0.859 err=0.251 valid=dirha_sim loss=1.748 err=0.405 lr_architecture1=0.000025 lr_architecture2=0.000025 time(s)=2838
ep=020 tr=['tr05_cont'] loss=0.855 err=0.250 valid=dirha_sim loss=1.745 err=0.405 lr_architecture1=0.000013 lr_architecture2=0.000013 time(s)=2874
ep=021 tr=['tr05_cont'] loss=0.852 err=0.249 valid=dirha_sim loss=1.745 err=0.404 lr_architecture1=0.000013 lr_architecture2=0.000013 time(s)=2810
ep=022 tr=['tr05_cont'] loss=0.849 err=0.248 valid=dirha_sim loss=1.745 err=0.405 lr_architecture1=0.000006 lr_architecture2=0.000006 time(s)=2734
ep=023 tr=['tr05_cont'] loss=0.848 err=0.248 valid=dirha_sim loss=1.746 err=0.405 lr_architecture1=0.000003 lr_architecture2=0.000003 time(s)=2801
%WER 25.3 | 409 6762 | 77.0 16.7 6.3 2.3 25.3 77.5 | /scratch/ravanelm/pytorch-kaldi-new/exp/dirha_GRU_fmllr_4lay/decode_dirha_real_out_dnn2/score_16/ctm.filt.sys

