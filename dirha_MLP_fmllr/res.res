ep=000 tr=['tr05_cont'] loss=2.915 err=0.654 valid=dirha_sim loss=2.952 err=0.656 lr_architecture1=0.080000 time(s)=542
ep=001 tr=['tr05_cont'] loss=2.210 err=0.558 valid=dirha_sim loss=2.812 err=0.635 lr_architecture1=0.080000 time(s)=418
ep=002 tr=['tr05_cont'] loss=2.050 err=0.529 valid=dirha_sim loss=2.735 err=0.617 lr_architecture1=0.080000 time(s)=419
ep=003 tr=['tr05_cont'] loss=1.954 err=0.511 valid=dirha_sim loss=2.721 err=0.612 lr_architecture1=0.080000 time(s)=420
ep=004 tr=['tr05_cont'] loss=1.886 err=0.497 valid=dirha_sim loss=2.716 err=0.609 lr_architecture1=0.080000 time(s)=429
ep=005 tr=['tr05_cont'] loss=1.835 err=0.487 valid=dirha_sim loss=2.715 err=0.606 lr_architecture1=0.080000 time(s)=429
ep=006 tr=['tr05_cont'] loss=1.793 err=0.479 valid=dirha_sim loss=2.687 err=0.603 lr_architecture1=0.080000 time(s)=438
ep=007 tr=['tr05_cont'] loss=1.758 err=0.472 valid=dirha_sim loss=2.710 err=0.604 lr_architecture1=0.080000 time(s)=442
ep=008 tr=['tr05_cont'] loss=1.699 err=0.460 valid=dirha_sim loss=2.636 err=0.593 lr_architecture1=0.040000 time(s)=435
ep=009 tr=['tr05_cont'] loss=1.668 err=0.454 valid=dirha_sim loss=2.651 err=0.592 lr_architecture1=0.040000 time(s)=438
ep=010 tr=['tr05_cont'] loss=1.650 err=0.450 valid=dirha_sim loss=2.672 err=0.595 lr_architecture1=0.040000 time(s)=422
ep=011 tr=['tr05_cont'] loss=1.623 err=0.444 valid=dirha_sim loss=2.631 err=0.590 lr_architecture1=0.020000 time(s)=447
ep=012 tr=['tr05_cont'] loss=1.605 err=0.440 valid=dirha_sim loss=2.638 err=0.587 lr_architecture1=0.020000 time(s)=437
ep=013 tr=['tr05_cont'] loss=1.595 err=0.438 valid=dirha_sim loss=2.643 err=0.588 lr_architecture1=0.020000 time(s)=458
ep=014 tr=['tr05_cont'] loss=1.582 err=0.435 valid=dirha_sim loss=2.629 err=0.585 lr_architecture1=0.010000 time(s)=431
ep=015 tr=['tr05_cont'] loss=1.572 err=0.433 valid=dirha_sim loss=2.618 err=0.586 lr_architecture1=0.010000 time(s)=449
ep=016 tr=['tr05_cont'] loss=1.565 err=0.431 valid=dirha_sim loss=2.627 err=0.585 lr_architecture1=0.005000 time(s)=433
ep=017 tr=['tr05_cont'] loss=1.560 err=0.430 valid=dirha_sim loss=2.623 err=0.585 lr_architecture1=0.005000 time(s)=426
ep=018 tr=['tr05_cont'] loss=1.556 err=0.430 valid=dirha_sim loss=2.621 err=0.583 lr_architecture1=0.002500 time(s)=454
ep=019 tr=['tr05_cont'] loss=1.553 err=0.429 valid=dirha_sim loss=2.624 err=0.584 lr_architecture1=0.002500 time(s)=449
ep=020 tr=['tr05_cont'] loss=1.552 err=0.429 valid=dirha_sim loss=2.616 err=0.582 lr_architecture1=0.001250 time(s)=469
ep=021 tr=['tr05_cont'] loss=1.551 err=0.428 valid=dirha_sim loss=2.618 err=0.584 lr_architecture1=0.001250 time(s)=466
ep=022 tr=['tr05_cont'] loss=1.551 err=0.428 valid=dirha_sim loss=2.623 err=0.583 lr_architecture1=0.000625 time(s)=429
ep=023 tr=['tr05_cont'] loss=1.549 err=0.428 valid=dirha_sim loss=2.618 err=0.583 lr_architecture1=0.000625 time(s)=437
%WER 26.1 | 409 6762 | 76.4 16.7 6.9 2.5 26.1 80.0 | /scratch/ravanelm/pytorch-kaldi-new/exp/dirha_MLP_fmllr/decode_dirha_real_out_dnn1/score_15/ctm.filt.sys

