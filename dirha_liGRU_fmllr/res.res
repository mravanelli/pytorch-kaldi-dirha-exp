ep=000 tr=['tr05_cont'] loss=3.784 err=0.684 valid=dirha_sim loss=2.424 err=0.546 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1340
ep=001 tr=['tr05_cont'] loss=1.920 err=0.493 valid=dirha_sim loss=2.069 err=0.477 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1448
ep=002 tr=['tr05_cont'] loss=1.594 err=0.428 valid=dirha_sim loss=1.931 err=0.451 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1773
ep=003 tr=['tr05_cont'] loss=1.450 err=0.399 valid=dirha_sim loss=1.885 err=0.441 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1946
ep=004 tr=['tr05_cont'] loss=1.352 err=0.377 valid=dirha_sim loss=1.830 err=0.432 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1965
ep=005 tr=['tr05_cont'] loss=1.273 err=0.360 valid=dirha_sim loss=1.808 err=0.425 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1938
ep=006 tr=['tr05_cont'] loss=1.210 err=0.346 valid=dirha_sim loss=1.813 err=0.425 lr_architecture1=0.000400 lr_architecture2=0.000400 time(s)=1949
ep=007 tr=['tr05_cont'] loss=1.108 err=0.322 valid=dirha_sim loss=1.762 err=0.412 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=1961
ep=008 tr=['tr05_cont'] loss=1.073 err=0.314 valid=dirha_sim loss=1.751 err=0.410 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=1940
ep=009 tr=['tr05_cont'] loss=1.044 err=0.307 valid=dirha_sim loss=1.756 err=0.407 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=1958
ep=010 tr=['tr05_cont'] loss=1.019 err=0.302 valid=dirha_sim loss=1.748 err=0.406 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=1949
ep=011 tr=['tr05_cont'] loss=0.996 err=0.296 valid=dirha_sim loss=1.745 err=0.408 lr_architecture1=0.000200 lr_architecture2=0.000200 time(s)=1945
ep=012 tr=['tr05_cont'] loss=0.951 err=0.285 valid=dirha_sim loss=1.743 err=0.403 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=1960
ep=013 tr=['tr05_cont'] loss=0.936 err=0.282 valid=dirha_sim loss=1.748 err=0.402 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=1953
ep=014 tr=['tr05_cont'] loss=0.924 err=0.279 valid=dirha_sim loss=1.746 err=0.403 lr_architecture1=0.000100 lr_architecture2=0.000100 time(s)=1964
ep=015 tr=['tr05_cont'] loss=0.902 err=0.273 valid=dirha_sim loss=1.747 err=0.401 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=1935
ep=016 tr=['tr05_cont'] loss=0.894 err=0.271 valid=dirha_sim loss=1.748 err=0.400 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=1947
ep=017 tr=['tr05_cont'] loss=0.885 err=0.269 valid=dirha_sim loss=1.745 err=0.398 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=1940
ep=018 tr=['tr05_cont'] loss=0.881 err=0.268 valid=dirha_sim loss=1.746 err=0.399 lr_architecture1=0.000050 lr_architecture2=0.000050 time(s)=1942
ep=019 tr=['tr05_cont'] loss=0.870 err=0.265 valid=dirha_sim loss=1.754 err=0.400 lr_architecture1=0.000025 lr_architecture2=0.000025 time(s)=1999
ep=020 tr=['tr05_cont'] loss=0.865 err=0.264 valid=dirha_sim loss=1.747 err=0.399 lr_architecture1=0.000013 lr_architecture2=0.000013 time(s)=2094
ep=021 tr=['tr05_cont'] loss=0.863 err=0.263 valid=dirha_sim loss=1.750 err=0.399 lr_architecture1=0.000013 lr_architecture2=0.000013 time(s)=2076
ep=022 tr=['tr05_cont'] loss=0.861 err=0.263 valid=dirha_sim loss=1.750 err=0.399 lr_architecture1=0.000006 lr_architecture2=0.000006 time(s)=2088
ep=023 tr=['tr05_cont'] loss=0.859 err=0.263 valid=dirha_sim loss=1.750 err=0.399 lr_architecture1=0.000003 lr_architecture2=0.000003 time(s)=2063
%WER 23.8 | 409 6762 | 77.7 14.2 8.1 1.5 23.8 77.3 | /scratch/ravanelm/pytorch-kaldi-new/exp/dirha_liGRU_fmllr_4lay/decode_dirha_real_out_dnn2/score_19/ctm.filt.sys

